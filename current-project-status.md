## Tila?

* date: 26.4.2018
* team: Alphanet
* cumulative-working-hours: 679
* current-bill: 33950€

## Miten homma etenee?

* Projekti on saatettu loppuun

## Ongelmakohdat?

* Ei ongelmia havaittu

## Mitä seuraavaksi?

* Seuraavia projekteja kohti

## Tärkeitä linkkejä 

* [OBSIMO](https://obsimo.cf)
* [Linkki tuntikirjauksiin?](https://gitlab.labranet.jamk.fi/Alphanet/projekti-01/wikis/projektin-aikataulu-ja-resurssointi)
* [Kotisivu](http://alphanet.pages.labranet.jamk.fi/projekti-01)
* [Youtub Video](https://youtu.be/2IC_8imCi7Y)
* [Loppuraportti](https://gitlab.labranet.jamk.fi/Alphanet/projekti-01/wikis/loppuraportti)